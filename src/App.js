import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Container } from 'reactstrap';
import Home from './components/SearchBar';
import SearchResults from './components/SearchResults';
import CompanySummary from './components/CompanySummary';
import NavBar from './components/common/NavBar';
import FourOhFour from './components/common/FourOhFour';

function App() {

  return (
    <Router data-test="app">
      <NavBar/>
      <Container>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/search/:query">
              <SearchResults />
            </Route>
            <Route exact path={'/company/:abn'}>
              <CompanySummary />
            </Route>
            <Route component={FourOhFour} />
          </Switch>
      </Container>
    </Router>
  );
}

export default App;
