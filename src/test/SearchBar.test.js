import * as React from 'react'
import * as Enzyme from 'enzyme';
import { MemoryRouter } from 'react-router';
import Adapter from 'enzyme-adapter-react-16';
import SearchBar from '../components/SearchBar';

Enzyme.configure({adapter: new Adapter()});

describe('SearchBar', () => {

  test('renders without error', () => {
    const component = Enzyme.mount(
      <MemoryRouter initialEntries={[ '/' ]}>
        <SearchBar/>
      </MemoryRouter>
    );
    expect(component.find(`[data-test="search-bar"]`).length).toBe(1);
  });
  
});