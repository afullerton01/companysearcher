import * as React from 'react'
import * as Enzyme from 'enzyme';
import { MemoryRouter } from 'react-router';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App';
import SearchBar from '../components/SearchBar';
import SearchResults from '../components/SearchResults';
import FourOhFour from '../components/common/FourOhFour';
import CompanySummary from '../components/CompanySummary';

Enzyme.configure({adapter: new Adapter()});

const testSearchResults = {
  Name: [],
  Message: ''
}

describe('App', () => {

  beforeAll(() => {
    global.fetch = fetch;
  });
  afterAll(() => {
    fetchMock.restore();
  });

  test('renders without error', () => {
    const component = Enzyme.mount(
      <MemoryRouter initialEntries={[ '/' ]}>
        <App/>
      </MemoryRouter>
    );
    expect(component.find(`[data-test="app"]`).length).toBe(1);
  });

  test('routes to SearchBar component at /', () => {
    const component = Enzyme.mount(
      <MemoryRouter initialEntries={[ '/' ]}>
        <SearchBar/>
      </MemoryRouter>
    );
    expect(component.find(`[data-test="search-bar"]`).length).toBe(1);
  });

  test('routes to SearchResults component at /search/:test', () => {
    const component = Enzyme.mount(
      <MemoryRouter initialEntries={[ '/search/test' ]}>
        <SearchResults/>
      </MemoryRouter>
    );
    expect(component.find(`[data-test="search-results"]`).length).toBe(1);
  });

  test('routes to CompanySummary component at /company/:testABN', async () => {
    const component = Enzyme.mount(
      <MemoryRouter initialEntries={[ '/search/111111' ]}>
        <CompanySummary/>
      </MemoryRouter>
    );
    expect(component.find(`[data-test="company-summary"]`).length).toBe(1);
  });

  test('routes to 404 component at unknown route', () => {
    const component = Enzyme.mount(
      <MemoryRouter initialEntries={[ '/unknownroute' ]}>
        <FourOhFour/>
      </MemoryRouter>
    );
    expect(component.find(`[data-test="four-oh-four"]`).length).toBe(1);
  });
  
});