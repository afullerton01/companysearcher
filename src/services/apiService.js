const fetchJsonp = require('fetch-jsonp');

class ApiService {

  API_BASE_URL = 'https://abr.business.gov.au/json';

  // Have used .env file to hide GUID from source control. If this app were to be used in a production environment
  // I would have created a server which handles the communication with abr.business.gov.au 
  // and accesses the GUID via env variable on the server. The client would then communicate with the 
  // server for all requests (and therefore not exposing the sensitive key).

  getCompanies(name) {
    return fetchJsonp(`${this.API_BASE_URL}/MatchingNames.aspx?name=${name}&guid=${process.env.REACT_APP_ABR_API_GUID}`)
    .then(function(response) {
      return response.json()
    }).catch(function(error) {
      console.log(error);
    });
  }

  getCompanyByABN(abn) {
    return fetchJsonp(`${this.API_BASE_URL}/AbnDetails.aspx?abn=${abn}&guid=${process.env.REACT_APP_ABR_API_GUID}`)
    .then(function(response) {
      return response.json()
    }).catch(function(error) {
      console.log(error);
    });
  }
  
}

export default new ApiService();