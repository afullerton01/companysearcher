import * as React from 'react';
import { compose } from 'recompose';
import { withRouter, Link } from 'react-router-dom';
import { generate as key } from 'shortid';
import { Table, Button} from 'reactstrap';
import ApiService from '../services/apiService';
import Loader from './common/Loader';

const SearchResults = (props) => {

  const [ isLoading, setIsLoading ] = React.useState(true);
  const [ searchResults, updateSearchResults ] = React.useState({});

  React.useEffect(() => {

    async function getSearchResults(query) {
      const searchResults = await ApiService.getCompanies(query);
      updateSearchResults({...searchResults})
      setIsLoading(false);
    }

    if(props.match.params.query) {
      getSearchResults(props.match.params.query);
    }

  }, [props]);

  if(isLoading) {
    return <Loader data-test="search-results"/>
  };

  if(searchResults.Message) {
    return (
      <div data-test="search-results">
        <h2>Search Results</h2>
        <div className="text-center m-5">{props.results.Message}</div>
        <Button onClick={props.history.goBack}>Go Back</Button>
      </div>
    );
  };

  if(searchResults.Names.length === 0) {
    return (
      <div data-test="search-results">
        <h2>Search Results</h2>
        <div className="text-center m-5">Sorry, no results found for '{props.match.params.query}'</div>
        <Button onClick={props.history.goBack}>Go Back</Button>
      </div>
    );
  };

  const searchResultRows = searchResults.Names.map(result => (
    <tr key={key()}>
      <td><Link to={`/company/${result.Abn}`}>{result.Name}</Link></td>
      <td>{result.Abn}</td>
      <td>{result.State}</td>
      <td>{result.Postcode}</td>
    </tr>
  ));

  return (
    <div data-test="search-results">
      <h2>Search Results</h2>
      <Table>
        <tbody>
          <tr>
            <th>Name</th>
            <th>ABN</th>
            <th>State</th>
            <th>Postcode</th>
          </tr>
          {searchResultRows}
        </tbody>
      </Table>
      <Button onClick={() => props.history.push({ pathname: '/'})}>New Search</Button>
    </div>
  );

};

export default compose(withRouter)(SearchResults);