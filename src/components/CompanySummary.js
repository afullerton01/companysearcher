import * as React from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import ApiService from '../services/apiService';
import { Button, Table} from 'reactstrap';
import Loader from './common/Loader';

const ABNSearchResult = (props) => {

  const [ isLoading, setIsLoading ] = React.useState(true);
  const [ searchResult, updateSearchResult ] = React.useState({});

  React.useEffect(() => {

    async function getSearchResult(abn) {
      const company = await ApiService.getCompanyByABN(abn);
      updateSearchResult({...company})
      setIsLoading(false);
    }

    if(props.match.params.abn) {
      getSearchResult(props.match.params.abn);
    }

  }, [props]);

  if(isLoading) {
    return <Loader data-test="company-summary"/>
  };

  if(searchResult.Message) {
    return (
      <div data-test="company-summary">
        <h2>Search Results</h2>
        <div className="text-center m-5">{searchResult.Message}</div>
        <Button onClick={props.history.goBack}>Go Back</Button>
      </div>
    );
  };

  return (
    <div data-test="company-summary">
      <h2>Search Results</h2>
      <Table>
        <tbody>
          <tr>
            <td>Entity Name:</td>
            <td>{searchResult.EntityName}</td>
          </tr>
          <tr>
            <td>Entity Type:</td>
            <td>{searchResult.EntityTypeName}</td>
          </tr>
          <tr>
            <td>ABN:</td>
            <td>{searchResult.Abn}</td>
          </tr>
          <tr>
            <td>ABN Status:</td>
            <td>{searchResult.AbnStatus}</td>
          </tr>
          <tr>
            <td>Postcode:</td>
            <td>{searchResult.AddressPostcode}</td>
          </tr>
          <tr>
            <td>State:</td>
            <td>{searchResult.AddressState}</td>
          </tr>
        </tbody>
      </Table>
      <Button onClick={() => props.history.push({ pathname: '/'})}>New Search</Button>
    </div>
  );
  
};

export default compose(withRouter)(ABNSearchResult);