import React from 'react';
import { Spinner, Row, Col } from 'reactstrap';

const Loader = () => {

  return (
    <div data-test="loader">
      <h2>Search Results</h2>
      <Row>
        <Col md={4}/>
        <Col md={4} className="text-center">
          <Spinner className="mb-5" />
        </Col>
        <Col md={4}/>
      </Row>
    </div>
  );
}

export default Loader;