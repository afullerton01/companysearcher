import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';

const Loader = () => {

  return (
    <Navbar color="light" light expand="md" className="mb-5">
          <NavbarBrand>Company Searcher</NavbarBrand>
    </Navbar>
  );
}

export default Loader;