import React from 'react';
import { compose } from 'recompose';
import { withRouter, Link} from 'react-router-dom';
import { Row, Col } from 'reactstrap';

const FourOhFour = (props) => {
  return (
    <div data-test="four-oh-four">
      <Row>
        <Col md={2}/>
        <Col md={8} className="text-center">
          <p>Whoops that page doesn't exist!{' '}<Link to="/">Click here to return home.</Link></p>
        </Col>
        <Col md={2}/>
      </Row>
    </div>
  );
}

export default compose(withRouter)(FourOhFour);