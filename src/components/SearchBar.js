import React from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { Row, Col, Input, Button } from 'reactstrap';

function SearchBar(props) {

  const [ search, updateSearch ] = React.useState({ query: '', type: 'company'});

  async function handleSubmit(e) { 
    e.preventDefault();

    if(search.type === 'company') {
      props.history.push({
        pathname: `/search/${search.query}`,
      });
    }

    if(search.type === 'abn') {
      props.history.push({
        pathname: `/company/${search.query}`,
      });
    }

  }

  return (
    <div data-test="search-bar">
      <p className="text-center mb-5">Enter a company (name or ABN) below to begin your search</p>
      <Row form>
        <Col md={6} className="mt-1">
            <Input type="text" name="city" id="exampleCity" value={search.searchQuery} onChange={(e) => updateSearch({ ...search, query: e.target.value })} />
        </Col>
        <Col md={1} className="align-self-center mt-1">
          Search By
        </Col>
        <Col md={3} className="mt-1">
            <Input type="select" name="select" value={search.type} onChange={(e) => updateSearch({...search, type: e.target.value})}>
              <option value="company">Company Name</option>
              <option value="abn">ABN</option>
            </Input>
        </Col>
        <Col md={2} className="mt-1">
            <Button block onClick={(e) => handleSubmit(e)} color="success" disabled={search.query.length === 0}>Search</Button>
        </Col>
      </Row>
    </div>
  );
}

export default compose(withRouter)(SearchBar);
