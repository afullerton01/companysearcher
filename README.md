## Company Search App

This app allows you to search for Australian companies via their name or ABN. 

## How to run 

### Local app

1. Clone repo

2. Install dependencies (from within cloned folder) `$ npm install`

3. Create .env file within main project folder. Include `REACT_APP_ABR_API_GUID=<GUID HERE>`

4. Run app `$ npm run start`

5. Visit http://localhost:3000/

## Testing

Basic component testing has been incorporated and can be run via the following command:

```console
$ npm run test
```

